use std::io::{BufRead, Write};
use std::str;

const CODEPAGE_437: [char; 128] = [
//   0    1    2    3    4    5    6    7    8    9    A    B    C    D    E    F    
    'Ç', 'ü', 'é', 'â', 'ä', 'à', 'å', 'ç', 'ê', 'ë', 'è', 'ï', 'î', 'ì', 'Ä', 'Å', // U+8x0
    'É', 'æ', 'Æ', 'ô', 'ö', 'ò', 'û', 'ù', 'ÿ', 'Ö', 'Ü', '¢', '£', '¥', '₧', 'ƒ', // U+9x0
    'á', 'í', 'ó', 'ú', 'ñ', 'Ñ', 'ª', 'º', '¿', '⌐', '¬', '½', '¼', '¡', '«', '»', // U+Ax0
    '░', '▒', '▓', '│', '┤', '╡', '╢', '╖', '╕', '╣', '║', '╗', '╝', '╜', '╛', '┐', // U+Bx0
    '└', '┴', '┬', '├', '─', '┼', '╞', '╟', '╚', '╔', '╩', '╦', '╠', '═', '╬', '╧', // U+Cx0
    '╨', '╤', '╥', '╙', '╘', '╒', '╓', '╫', '╪', '┘', '┌', '█', '▄', '▌', '▐', '▀', // U+Dx0
    'α', 'ß', 'Γ', 'π', 'Σ', 'σ', 'µ', 'τ', 'Φ', 'Θ', 'Ω', 'δ', '∞', 'φ', 'ε', '∩', // U+Ex0
    '≡', '±', '≥', '≤', '⌠', '⌡', '÷', '≈', '°', '∙', '·', '√', 'ⁿ', '²', '■', ' ', // U+Fx0
];

const BUF_SIZE: usize = 8192;

pub fn to_utf8<R,W>(src: &mut R, dest: &mut W)
where
    R: BufRead,
    W: Write
{
    let mut buf: Vec<u8> = vec![0;BUF_SIZE];

    while let Ok(amount) = src.read(&mut buf) {
        if amount == 0 {
            break;
        }
        
        let mut bufw: Vec<u8> = vec![];
        for i in 0..amount {
            if buf[i] > 127 {
                let c = CODEPAGE_437[(buf[i] - 128) as usize];
                let mut bufr: Vec<u8> = vec![0;2];
                c.encode_utf8(&mut bufr);
                //println!("{} - {:?}", c, bufr);
                bufw.append(&mut bufr);
            } else {
                bufw.push(buf[i]);
            }
        }
        //println!("{:?}", bufw);
        dest.write(&bufw);
        
    }
}
