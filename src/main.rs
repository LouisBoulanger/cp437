extern crate codepoint;

use std::fs::File;
use std::io::{BufRead, BufReader, BufWriter, Write};
use std::time::{Duration, Instant};

fn main() {
    let file = File::open("sample.xml").unwrap();
    let out = File::create("sample_out.xml").unwrap();

    let mut file = BufReader::new(file);
    let mut out = BufWriter::new(out);

    // Discard first line to set own encoding
    out.write("<?xml version=\"1.0\" encoding='utf-8'?>".as_bytes());
    let mut buf = String::new();
    file.read_line(&mut buf);

    codepoint::to_utf8(&mut file, &mut out);
}
